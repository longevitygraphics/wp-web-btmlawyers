<?php


include 'cpt.php';

// MOVE YOAST META BOX TO BOTTOM
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');





/* ENABLE THEME CUSTOMIZER */
include 'theme_customizer.php';





/* ADD FEATURED IMAGE AREA */
add_theme_support( 'post-thumbnails' );







/* ADD CUSTOM STYLESHEET/STYLES TO THE WYSIWYG EDITOR */
add_editor_style('wysiwig.css');







/* REGISTER MENU */

if ( function_exists( 'register_nav_menus' ) ) {
      register_nav_menus(
          array(
            'primary-menu' => 'Primary Menu',
          'footer-menu' => 'Footer Menu'
          )
      );
}








/* EXCERPT LENGTH */

function custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );









/* EXCERPT ENDING ... */

    function change_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'change_excerpt_more');








/* IF PAGE IS PARENT OR CHILD - use - (is_tree(2)) */

function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;         // load details about this page
    if(is_page()&&($post->post_parent==$pid||is_page($pid)))
               return true;   // we're at the page or at a sub page
    else
               return false;  // we're elsewhere
};










/* REGISTER PAGINATION */
/* http://www.kriesi.at/archives/how-to-build-a-wordpress-post-pagination-without-plugin */

function kriesi_pagination($pages = '', $range = 2)
{
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class='pagination-wrap'>\n";
         echo "<div class='pagination'>\n";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
         echo "</div>\n";
     }
};



// POSTS TO POSTS plugin

function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'services_to_lawyers',
        'from' => 'page',
        'to' => 'page',
        //'reciprocal' => true,
        'title' => 'Connect Lawyer to Practice',
        'sortable' => 'any',
        'admin_box' => 'from'
    ) );
}
add_action( 'p2p_init', 'my_connection_types' );



// Enqueue css
function enqueue_styles_scripts() {
    wp_enqueue_style( 'landing-page-css', get_stylesheet_directory_uri().'/css/landing-page.css', array(), '4.9.1', 'all' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_scripts' );


/*dequeue wp-embed.js*/
function lg_deregister_embed(){
	wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'lg_deregister_embed' );
