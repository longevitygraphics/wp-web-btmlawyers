<?php get_header(); ?>



    <!-- START CONTENT -->
    <?php  
        $page_parent = get_the_title( $post->post_parent );
    ?>
    <?php if (0 != $post->post_parent && $page_parent == 'Services' ): ?>
    <div class="service-page-top">
        <?php if (is_page('personal-injury')): ?>
            <?php 
                $args = array(
                    'orderby'          => 'title',
                    'order'            => 'ASC',
                    'posts_per_page'   => -1,
                    'post_type'        => 'subpages',
                    'post_status'      => 'publish',
                    'suppress_filters' => true
                );
            ?>
            <div class="subpages-cta" id="pi-subpages">
                <?php
                $subPagesArray = get_posts( $args );
                foreach ($subPagesArray as $spage) {
                    $sTitle = get_field('link_title', $spage->ID);
                    echo '<a class="pi-subpage-link wow fadeIn" href="'.get_permalink($spage->ID).'">'.$sTitle.'</a>';
                }
                ?>
            </div>

        <?php endif; ?>
        <?php if (is_page('family-law')): ?>
        <?php 
            $args = array(
                'orderby'          => 'title',
                'order'            => 'ASC',
                'posts_per_page'   => -1,
                'post_type'        => 'family-law-subpages',
                'post_status'      => 'publish',
                'suppress_filters' => true
            );
        ?>
        <div id="pi-subpages">
            <?php
                $subPagesArray = get_posts( $args );
                foreach ($subPagesArray as $spage) {
                    $sTitle = get_field('link_title', $spage->ID);
                    echo '<a class="pi-subpage-link wow fadeIn" href="'.get_permalink($spage->ID).'">'.$sTitle.'</a>';
                }
            ?>
        </div>  
        <?php endif; ?>

        <?php if (is_page('employment')): ?>
            <?php 
                $args = array(
                    'orderby'          => 'title',
                    'order'            => 'ASC',
                    'posts_per_page'   => -1,
                    'post_type'        => 'employment-subpages',
                    'post_status'      => 'publish',
                    'suppress_filters' => true
                );
            ?>
            <div id="pi-subpages">
                <?php
                $subPagesArray = get_posts( $args );
                foreach ($subPagesArray as $spage) {
                    $sTitle = get_field('link_title', $spage->ID);
                    echo '<a class="pi-subpage-link wow fadeIn" href="'.get_permalink($spage->ID).'">'.$sTitle.'</a>';
                }
                ?>
            </div>
        <?php endif; ?>

        <?php if (get_field('contact_form') && !is_page('trademarks')): ?>
            <div class="internal-service-form">
                <?php if (get_field('contact_form_title')): ?>
                    <h2 class="interna-service-form-title"><?php echo get_field('contact_form_title'); ?></h2>
                <?php endif; ?>
                <div class="help-form-box">
                    <?php echo do_shortcode(get_field('contact_form')); ?>
                </div>
            </div>
        <?php endif; ?>
    </div> <!-- end of service page top content -->
    <?php endif // end if post parent is service?>

    <div class="content-wrap">
        
        
    <?php if(is_page(9) | is_page(7)) /* If Team or Service Index Pages */ :?>
        <div class="content-wide">
    <?php else :?>
        <div class="content">
    <?php endif;?>

        
        
    
        
    <?php if(is_front_page()) /* If Home Page */ :?>
        
    <!-- do nothing -->
        
    <?php elseif(is_page(11)) /* If About Page */ :?>
        
    <!-- do nothing -->

    <?php elseif(is_page(15)) /* If Contact Page */ :?>
        
    <!-- do nothing -->
        
    <?php elseif(is_page(7)) /* If Service Page */ :?>
        
    <h1><?php the_title(); ?></h1>
        
    <?php elseif(is_tree(7)) /* If Service Sub Pages */ :?>
        
    <!-- do nothing -->
        
    <?php else :?>
        
    <h1><?php the_title(); ?></h1>
        
    <?php endif;?>
        
            
            
            
            
      
    <?php if(is_page(7)) /* If Services Page */ :?>
        
        
    <?php if(has_post_thumbnail()) :?>
            
    <style type="text/css">
    .content-wrap {
        background-image: url('<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); echo $src[0]; ?>');
        background-repeat: no-repeat;
        background-position: center center;
        background-size: cover;
    }
    </style>
            
    <?php endif;?>
          
            
        
    <div id="spacer"></div>
        
    <?php 

	// Get the page's children (no grandchildren)
	$children = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'menu_order', 'parent' => $post->ID ) );

	if (!empty($children)) { 
		foreach($children as $child) {
		
			// Get title and link
			$title = $child->post_title;
			$link = get_permalink( $child );
            
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
			
			// Get advanced custom field data
			$icon = get_field('icon', $pageid);
            $icon_text = get_field('icon_text', $pageid); 
            
			echo '<div class="service-teaser-box wow fadeInUp" data-wow-delay="400ms">';            
            echo '<a class=" ' . (($icon_text)? 'icon-text':'') .' service-teaser-icon" href="' . $link . '"><span class="fa ' . $icon . ' fa-4x">' . (($icon_text)? $icon_text:'') . '</span></a><div class="service-teaser-heading">' . $title . '</div>';
			echo '</div>';
			
		}
	}        
	?>
        
    <?php endif;?>
    
    <br><br>
            
    <?php wp_reset_query(); ?>
            
            
            
            
            
            
    <?php if(is_page(9)) /* If Team Page */ :?>
    
    <?php 

	// Get the page's children (no grandchildren)
	$children = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'menu_order', 'parent' => $post->ID ) );
    
	if (!empty($children)) { 
		foreach($children as $child) {
		
			// Get title and link
			$title = $child->post_title;
			$link = get_permalink( $child );
                
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
			
			// Get advanced custom field data
			$indexcontent = get_field('index_excerpt', $pageid); 
			$indeximage = get_field('index_image', $pageid);
            $lawyer_title = get_field( 'title', $pageid );
			
            /* ======= This was changed because WOW.js was not displaying lawyer bios in IE10 ======= */
            // echo '<div class="lawyer-teaser-large wow fadeInUp" data-wow-delay="400ms">';
			// echo '<div class="lawyer-teaser-large">'; /* This works in IE10 */
            echo '<div class="lawyer-teaser-large animated fadeInUp">'; /* Test for IE 10 */
            /* ============================= */


            echo '<div id="lawyer-teaser-photo"><a href="' . $link . '"><img src="' . $indeximage . '"></a></div>';
            echo '<div id="lawyer-teaser-content">';
            echo '<h2>' . $title; 
            if ( $lawyer_title == 'Articled Student' ) { echo ' <span>' . $lawyer_title . '</span>' ; }
            echo '</h2>';
            echo '<p id="lawyer-teaser-paragraph">' . $indexcontent . '</p>';
            echo '<p><a href="' . $link . '">View bio</a></p>';
            echo '</div>';
			echo '</div>';
			
		}
	}        
	?>

    <?php endif;?>
            
            
            
            
        

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
           
    <?php if(is_tree(7) && !is_page(7))  /* If Service Sub Pages */  { ?>
        <h2><?php the_title()?> Team</h2>
            
        <?php
            // Find connected pages
            $connected = new WP_Query( array(
              'connected_type' => 'services_to_lawyers',
              'connected_items' => get_queried_object(),
              'nopaging' => true,
            ) );

            // Display connected pages
            if ( $connected->have_posts() ) :  ?>
            <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
            
            <?php
               // Get title and link
                $title = $post->post_title;
                $link = get_permalink( $child );

                // Get page ID from link for ACF
                $pageid = url_to_postid( $link );

                // Get advanced custom field data
                $indexcontent = get_field('index_excerpt', $pageid); 
                $indeximage = get_field('index_image', $pageid);
                $lawyer_title = get_field( 'title', $pageid );
                                                                      
                /* ======= This was changed because WOW.js was not displaying lawyer bios in IE10 ======= */
                // echo '<div class="lawyer-teaser-large wow fadeInUp" data-wow-delay="400ms">';
                // echo '<div class="lawyer-teaser-large">'; /* This works in IE10 */
                echo '<div class="lawyer-teaser-large animated fadeInUp">'; /* Test for IE 10 */
                /* ============================= */


                echo '<div id="lawyer-teaser-photo"><a href="' . $link . '"><img src="' . $indeximage . '"></a></div>';
                echo '<div id="lawyer-teaser-content">';
                echo '<h3 style="text-align:left;margin:0;">' . $title;
                if ( $lawyer_title == 'Articled Student' ) { echo ' <span>' . $lawyer_title . '</span>' ; }
                echo '</h3>';
                echo '<p id="lawyer-teaser-paragraph">' . $indexcontent . '</p>';
                echo '<p><a href="' . $link . '">View bio</a></p>';
                echo '</div>';
                echo '</div>';
            ?>
            
            <?php endwhile; ?>
            

            <?php wp_reset_postdata();
            endif; ?>
            
        
    <?php } ?>
            
	<?php endwhile; endif; ?>
        

    
        

        

    </div>
    </div>
    <!-- END CONTENT -->
    
    
    
<?php get_footer(); ?>