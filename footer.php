    <?php if(is_front_page()) /* If Home Page */ :?>


    <!-- START TESTIMONIALS -->
    <div class="testimonials-wrap">
    <div class="testimonials">   
        
    <?php
    echo do_shortcode( '[show_testimonials]' );
    ?>

    </div>
    </div>
    <!-- END TESTIMONIALS -->


    <?php endif;?>







    <?php if(is_front_page()) /* If Home Page */ :?>
    
    <!-- START TEAM TEASER LARGE -->
    <div class="team-teaser-wrap">
    <div class="team-teaser">

	<h2 class="h1">Our Team</h2> 
        
        
    <?php 

	// Get the page's children (no grandchildren)
	$children = get_pages( array( 'child_of' => 9, 'sort_column' => 'menu_order', 'parent' => 9 ) );

	if (!empty($children)) { 
		foreach($children as $child) {
		
			// Get title and link
			$title = $child->post_title;
			$link = get_permalink( $child );
            
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
			
			// Get advanced custom field data
			$indexcontent = get_field('index_excerpt', $pageid); 
			$indeximage = get_field('index_image', $pageid);
			$lawyer_title = get_field( 'title', $pageid );
            
            echo '<div class="lawyer-teaser-large wow fadeInUp" data-wow-delay="400ms">';
            echo '<div id="lawyer-teaser-photo"><a href="' . $link . '"><img src="' . $indeximage . '"></a></div>';
            echo '<div id="lawyer-teaser-content">';
            echo '<h2>' . $title; 
            if ( $lawyer_title == 'Articled Student' ) { echo ' <span>' . $lawyer_title . '</span>' ; }
            echo '</h2>';
            echo '<p id="lawyer-teaser-paragraph">' . $indexcontent . '</p>';
            echo '<p><a href="' . $link . '">View bio</a></p>';
            echo '</div>';
			echo '</div>';
			
		}
	}        
	?>


    </div>
    </div>
    <!-- END TEAM TEASER LARGE -->

    <?php endif;?>






    <?php if(is_tree(9) && !is_page(9)) /* If Team is Parent and NOT Team Page  */ :?>

    <!-- START TEAM TEASER SMALL -->
    <div class="team-teaser-wrap">
    <div class="team-teaser">

	<h1>Our Team</h1>

        
    <?php 

	// Get the page's children (no grandchildren)
	$children = get_pages( array( 'child_of' => 9, 'sort_column' => 'menu_order', 'parent' => 9 ) );

	if (!empty($children)) { 
		foreach($children as $child) {
		
			// Get title and link
			$title = $child->post_title;
			$link = get_permalink( $child );
            
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
			
			// Get advanced custom field data
			$indexcontent = get_field('index_excerpt', $pageid); 
			$indeximage = get_field('index_image', $pageid);
			$lawyer_title = get_field( 'title', $pageid );
            
			echo '<div class="lawyer-teaser-small wow fadeInUp" data-wow-delay="400ms">';
            echo '<div id="lawyer-teaser-photo"><a href="' . $link . '"><img src="' . $indeximage . '"></a></div>';
            echo '<h4>' . $title; 
            if ( $lawyer_title == 'Articled Student' ) { echo ' <span>' . $lawyer_title . '</span>' ; }
            echo '</h4>';
			echo '</div>';
			
		}
	}        
	?>
 

    </div>
    </div>
    <!-- END TEAM TEASER SMALL -->

    <?php endif;?>





    <?php if(is_page(17)) /* If Personal Injury Page */ :?>

    <!-- START SUB PAGES -->
    <div class="news-teaser-wrap">
    <div class="news-teaser">
        
        
        
    <?php 

	// Get the page's children (no grandchildren)
	$children = get_pages( array( 'child_of' => 17, 'sort_column' => 'menu_order', 'parent' => 17 ) );

	if (!empty($children)) { 
		foreach($children as $child) {
		
			// Get title and link
			$title = $child->post_title;
			$link = get_permalink( $child );
            
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
			
			// Get advanced custom field data
			$excerpt = get_field('excerpt', $pageid); 
			
			echo '<div class="news-teaser-box wow fadeInUp" data-wow-delay="200ms">';
            echo '<h3>' . $title . '</h3>';
            echo '<p>' . $excerpt . '</p>';
            echo '<a class="btn-arrow" href="' . $link . '"><span class="fa fa-chevron-right"></span></a>';
			echo '</div>';
			
		}
	}        
	?>
        


    </div>
    </div>
    <!-- END SUB PAGES -->

    <?php endif;?>






    <?php if(is_front_page()) /* If Home Page */ :?>
    
    <style type="text/css">
    .personal-injury-teaser-wrap {
        background-image: url('<?php bloginfo('template_url'); ?>/images/personal-injury-teaser-bg.jpg');
        background-repeat: no-repeat;
        background-position: top center;
        background-size: cover;
    }
        
    @media screen and (max-width: 600px){
       .personal-injury-teaser-wrap {
            background-image: url('<?php bloginfo('template_url'); ?>/images/personal-injury-teaser-bg-mobile.jpg');
        }  
    }
    </style>
    
    <!-- START PERSONAL INJURY TEASER -->
    <div class="personal-injury-teaser-wrap">
    <div class="personal-injury-teaser">

	
	<h2 class="h1"><?php echo get_theme_mod( 'heading' ); ?></h2>
    
    <p><?php echo get_theme_mod( 'content' ); ?></p>
        
    <div id="button" class="wow fadeIn" data-wow-delay="600ms"><a href="<?php echo get_theme_mod( 'button-link' ); ?>"><?php echo get_theme_mod( 'button' ); ?></a></div>

    </div>
    </div>
    <!-- END PERSONAL INJURY TEASER -->

    <?php endif;?>





    <?php if(is_page(11)) /* If About Page */ :?>
    
    <style type="text/css">
    .personal-injury-teaser-wrap {
        background-image: url('<?php bloginfo('template_url'); ?>/images/about-teaser-bg.jpg');
        background-repeat: no-repeat;
        background-position: top center;
        background-size: cover;
    }
    </style>
    
    <!-- START ABOUT TEASER -->
    <div class="personal-injury-teaser-wrap">
    <div class="personal-injury-teaser">

	
	<h1><?php echo get_theme_mod( 'about-heading' ); ?></h1>
    
    <p><?php echo get_theme_mod( 'about-content' ); ?></p>
        
    <div id="button" class="wow fadeIn" data-wow-delay="600ms"><a href="<?php echo get_theme_mod( 'about-button-link' ); ?>"><?php echo get_theme_mod( 'about-button' ); ?></a></div>

    </div>
    </div>
    <!-- END ABOUT TEASER -->

    <?php endif;?>





    <?php if(is_page(17)) /* If Personal Injury Page */ :?>
    
    <style type="text/css">
    .personal-injury-teaser-wrap {
        /*background-image: url('<?php bloginfo('template_url'); ?>/images/personal-injury-teaser-bg.jpg');*/
        background-image: url('<?php bloginfo('template_url'); ?>/images/dummy-section-teaser-bg.jpg');
        background-repeat: no-repeat;
        background-position: top center;
        background-size: cover;
        color: #fff;
    }
    .personal-injury-teaser-wrap h1 {
        padding: 0;
        margin-bottom: 20px;
        color: #fff;
    }
    .personal-injury-teaser-wrap h2 {
        color: #fff;
        font-size: 22px;
        padding: 0;
        margin: 40px auto 20px;
    }
    .personal-injury-teaser-wrap p {
        color: #fff;
    }
    .personal-injury-teaser-wrap #button {
        padding-top: 10px;
        padding-bottom: 0;
    }    
    .personal-injury-teaser-wrap #button a:link, .personal-injury-teaser-wrap #button a:visited {
        background-color: transparent;
        border: 2px solid #fff;
        display: inline-block;
    }
    .personal-injury-teaser-wrap #button a:hover, .personal-injury-teaser-wrap #button a:active {
        color: #fff !important;
        border: 2px solid #104b7d;
        background-color: #104b7d;
        
        
        }
        
    @media screen and (max-width: 600px){
   .personal-injury-teaser-wrap {
        /*background-image: url('<?php bloginfo('template_url'); ?>/images/personal-injury-teaser-bg-mobile.jpg');*/
        background-image: url('<?php bloginfo('template_url'); ?>/images/dummy-section-teaser-bg-mobile.jpg');
    }  
    }
    </style>
    
    <!-- START PERSONAL INJURY TEASER -->
    <div class="personal-injury-teaser-wrap">
    <div class="personal-injury-teaser">

	<!-- Commenting out the fields, I am hardcoding the Dummy Section here... CS
	<h1><?php echo get_field( 'feature_heading' ); ?></h1>
    
    <p><?php echo get_field( 'feature_content' ); ?></p>
        
    <div id="button" class="wow fadeIn" data-wow-delay="600ms"><a href="tel:+1<?php echo get_field( 'feature_button' ); ?>"><span class="fa fa-mobile fa-lg valign-button-icon"></span> <?php echo get_field( 'feature_button' ); ?></a></div>-->
    
        <h1>Don’t be a Dummy</h1>
        <h2 style="margin-top: 0; margin-bottom: 50px;">ICBC claim? Talk to one of our lawyers.</h2>
        <p>ICBC hopes you won’t hire a lawyer to help you with your claim. That’s a pretty good reason why you should.</p>
        <p>Reduce the hassle of dealing with ICBC by putting our experienced personal injury lawyers to work for you. We’ll let you focus on your recovery, provide access to a full range of treatment options, and handle settlement negotiations on your behalf.</p>
        <p><b>Get smart. Get BTM Lawyers on your side.</b></p>
        <h2>Book a free consultation.</h2>
        <div id="button" class="wow fadeIn" data-wow-delay="600ms"><a href="tel:+1<?php echo get_field( 'feature_button' ); ?>"><span class="fa fa-mobile fa-lg valign-button-icon"></span> <?php echo get_field( 'feature_button' ); ?></a></div>

    </div>
    </div>
    <!-- END PERSONAL INJURY TEASER -->

    <?php endif;?>

    
    
    
    
    <?php if(is_front_page() || is_page(11) || is_tree(7) && !is_page(7) && !is_page(17)) /* If Home or About or Service Sub Pages (except PI) */ :?>
    
    <!-- START SERVICES TEASER -->
    <div class="services-teaser-wrap">
    <div class="services-teaser">

	<h2 class="h1">Our Legal Services</h2>
        
        
    <?php 

	// Get the page's children (no grandchildren)
	$children = get_pages( array( 'child_of' => 7, 'sort_column' => 'menu_order', 'parent' => 7 ) );

	if (!empty($children)) { 
		foreach($children as $child) {
		
			// Get title and link
			$title = $child->post_title;
			$link = get_permalink( $child );
            
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
			
			// Get advanced custom field data
			$icon = get_field('icon', $pageid);
            $icon_text = get_field('icon_text', $pageid);  
			
			echo '<div class="service-teaser-box wow fadeInUp" data-wow-delay="400ms">';            
            echo '<a class="' . (($icon_text)? 'icon-text':'') .' service-teaser-icon" href="' . $link . '"><span class="fa ' . $icon . ' fa-4x">' . (($icon_text)? $icon_text:'') . '</span></a><a class="service-archive-text" href="' . $link . '"><h3 class="service-teaser-heading">' . $title . '</h3></a>';
			echo '</div>';
			
		}
	}        
	?>


    </div>
    </div>
    <!-- END SERVICES TEASER -->

    <?php endif;?>
    

    

    
    <!-- <?php if(is_page(17))  :?> If Personal Injury Page  -->
    
    <!-- START HELP FORM -->
    <!-- <div class="help-form-wrap wow fadeInUp" data-wow-delay="400ms">
    <div class="help-form">

	<h1>How can we help you?</h1>
        

    <div class="help-form-box">        
    
    <?php echo do_shortcode( '[contact-form-7 id="235" title="How can we help you"]' ); ?>
        
    </div>


    </div>
    </div> -->
    <!-- END HELP FORM -->

    <!-- <?php endif;?> -->





    
    <!-- START CONTACT -->

    <style type="text/css">
        .contact-wrap {
            background-image: url('<?php bloginfo('template_url'); ?>/images/footer-map-bg.jpg');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: cover;
        }
    </style>

    <!-- ALTERNATE CONTACT ON CONTACT PAGE -->
    <?php if(is_page(15)) /* If Contact Page */ :?>

    <style type="text/css">
        .content-wrap {display: none;}
        
        p { 
            font-size: 16px;
            max-width: 80%;
            line-height: 1.6;
        }
    </style>

        <div class="contact-wrap">
            <div class="contact">
            <h3><?php echo get_theme_mod( 'address' ); ?></h3>
            <div id="button"><a href="<?php echo get_field( 'map_button_link' ); ?>" target="_blank"><i class="fa fa-map-marker fa-fw fa-lg valign-button-icon"></i><?php echo get_field( 'map_button' ); ?></a>
            </div>
            <p>Tel: <?php echo get_theme_mod( 'telephone' ); ?>  Fax: <?php echo get_theme_mod( 'fax' ); ?></p>
            <p><a href="mailto:<?php echo get_theme_mod( 'email' ); ?>"><?php echo get_theme_mod( 'email' ); ?></a></p>

            <?php echo get_field( 'content' ); ?>
                
            </div>
        </div>
        
    <?php else :?>
    <!-- ALTERNATE CONTACT ON CONTACT PAGE -->
    
    <div class="contact-wrap">
        <div class="contact">
            <h3 class="h1">Contact</h3>
            <p><?php echo get_theme_mod( 'address' ); ?></p>
            <p>Tel: <?php echo get_theme_mod( 'telephone' ); ?><br/>Fax: <?php echo get_theme_mod( 'fax' ); ?></p>

            <p><a href="mailto:<?php echo get_theme_mod( 'email' ); ?>"><?php echo get_theme_mod( 'email' ); ?></a></p>
                
            <div id="contact-button-wrap" class="wow fadeIn" data-wow-delay="600ms">
                <div id="button"><a href="<?php echo get_theme_mod( 'button-1-link' ); ?>"><?php echo get_theme_mod( 'button-1' ); ?></a></div>
                <div id="button"><a href="<?php echo get_theme_mod( 'button-2-link' ); ?>"><?php echo get_theme_mod( 'button-2' ); ?></a></div>
            </div>
        </div>
    </div> 

    <?php endif;?>
    <!-- END CONTACT -->

    <?php if(is_page(15)) /* If Contact Page */ :?>
    
    <!-- START HELP FORM -->
    <div class="contact-form-wrap wow fadeInUp" id="contact-form" data-wow-delay="400ms">
    <div class="contact-form">

	<h1>How can we help you?</h1>
        

    <div class="contact-form-box">        
    
    <?php echo do_shortcode( '[contact-form-7 id="546" title="Contact"]' ); ?>
        
    </div>


    </div>
    </div>
    <!-- END HELP FORM -->

    <?php endif;?>
    
    
    

    
    <!-- START SOCIAL -->
    <div class="social-wrap">

    <?php if( get_theme_mod( 'social_facebook') != '' ) : ?><a href="<?php echo get_theme_mod( 'social_facebook' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-facebook.png"></a><?php endif; ?>
    <?php if( get_theme_mod( 'social_twitter') != '' ) : ?><a href="<?php echo get_theme_mod( 'social_twitter' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-twitter.png"></a><?php endif; ?>
    <?php if( get_theme_mod( 'social_linkedin') != '' ) : ?><a href="<?php echo get_theme_mod( 'social_linkedin' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-linkedin.png"></a><?php endif; ?>
    <?php if( get_theme_mod( 'social_google') != '' ) : ?><a href="<?php echo get_theme_mod( 'social_google' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-google.png"></a><?php endif; ?>

    </div>
    <!-- END SOCIAL -->
    
    
    <!-- START PAY ONLINE -->
    <style>
        .pay-online {
            padding: 16px;
            text-align: center;
        }
    </style>
    <div class="pay-online">
        <span id="button"><a href="<?php echo home_url( '/pay/' ); ?>">Pay Online</a></span>
    </div>
    <!-- END PAY ONLINE -->
    
    
    <!-- START FOOTER NOTICE -->
    <div class="footer-notice-wrap">

    <?php echo get_theme_mod( 'slogan' ); ?>

    </div>
    <!-- END FOOTER NOTICE -->





    <!-- START FOOTER -->
    <div class="footer-wrap">
        
    <div class="footer-nav">
    <?php wp_nav_menu( array('menu' => 'Footer Menu', 'container_class' => 'footer-menu' )); ?>
    </div>

    <div id="copyright-credit">&#169; <?php echo date("Y") ?> <?php echo get_theme_mod( 'copyright' ); ?> 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Design: <a href="http://www.skunkworks.ca" rel="nofollow" target="_blank">Skunkworks Creative Group Inc.</a></div>

    </div>
    <!-- END FOOTER -->




<?php if(is_front_page()) /* If Home Page */ :?>

</div>
<!-- END CONTENT BELOW HEADER IMAGE -->

<?php endif;?>




<?php wp_footer(); ?>

</body>
</html>