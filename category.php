<?php get_header(); ?>



    <!-- START CONTENT -->
    <div class="content-wrap">
    <div class="content">

        
    <h1><?php echo get_field( 'blog_name', 13 ); ?></h1>
        
    <h2><?php single_cat_title(''); ?></h2>
        
      
    
        
        
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
        <?php if ( has_post_thumbnail() ) : ?>
            <div id="blog-index-thumbnail">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail( 'medium', array( 'class' => 'alignleft' ) ); ?>
			</a>
            </div>
		<?php endif; ?>
	
		<div id="date"><?php the_time('F jS, Y') ?></div>
	
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

		<?php the_excerpt(); ?>
		
		<div id="button"><a href="<?php the_permalink() ?>">View more</a></div>
		
	<?php endwhile; endif; ?>

	
	<!-- pagination - see functions.php --><?php kriesi_pagination(); ?>
        
    <div id="spacer"></div>
    
    <div id="button"><a href="<?php bloginfo('siteurl'); ?>/blog/">Back to blog</a></div>
        

    </div>
    </div>
    <!-- END CONTENT -->
    
    
    
<?php get_footer(); ?>