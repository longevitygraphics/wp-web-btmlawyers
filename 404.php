<?php get_header(); ?>



    <!-- START CONTENT -->
    <div class="content-wrap">
    <div class="content">

        
        
    <h1>Page Not Found</h1>

    
    <br><br><br>

    <p>Unfortunately, it looks like you've come across a page that does not exist.</p>
	<p>Please <a href="<?php bloginfo('home'); ?>">Click here</a> to return to our home page.</p>
        
    <br><br><br><br><br><br><br><br><br>
    
        

    </div>
    </div>
    <!-- END CONTENT -->




    <style type="text/css">
    .personal-injury-teaser-wrap {
        background-image: url('<?php bloginfo('template_url'); ?>/images/personal-injury-teaser-bg.jpg');
        background-repeat: no-repeat;
        background-position: top center;
        background-size: cover;
    }
        
    @media screen and (max-width: 600px){
   .personal-injury-teaser-wrap {
        background-image: url('<?php bloginfo('template_url'); ?>/images/personal-injury-teaser-bg-mobile.jpg');
    }  
    }
    </style>
    
    <!-- START PERSONAL INJURY TEASER -->
    <div class="personal-injury-teaser-wrap">
    <div class="personal-injury-teaser">

	
	<h1><?php echo get_theme_mod( 'heading' ); ?></h1>
    
    <p><?php echo get_theme_mod( 'content' ); ?></p>
        
    <div id="button" class="wow fadeIn" data-wow-delay="600ms"><a href="<?php echo get_theme_mod( 'button-link' ); ?>"><?php echo get_theme_mod( 'button' ); ?></a></div>

    </div>
    </div>
    <!-- END PERSONAL INJURY TEASER -->




        
    <!-- START SERVICES TEASER -->
    <div class="services-teaser-wrap">
    <div class="services-teaser">

	<h1>Services</h1>
        
        
    <?php 

	// Get the page's children (no grandchildren)
	$children = get_pages( array( 'child_of' => 7, 'sort_column' => 'menu_order', 'parent' => 7 ) );

	if (!empty($children)) { 
		foreach($children as $child) {
		
			// Get title and link
			$title = $child->post_title;
			$link = get_permalink( $child );
            
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
			
			// Get advanced custom field data
			$icon = get_field('icon', $pageid);
            $icon_text = get_field('icon_text', $pageid);   
			
			echo '<div class="service-teaser-box wow fadeInUp" data-wow-delay="400ms">';            
            echo '<a class="' . (($icon_text)? 'icon-text':'') .' service-teaser-icon" href="' . $link . '"><span class="fa ' . $icon . ' fa-4x">' . (($icon_text)? $icon_text:'') . '</span></a><div class="service-teaser-heading">' . $title . '</div>';
			echo '</div>';
			
		}
	}        
	?>


    </div>
    </div>
    <!-- END SERVICES TEASER -->

    
    
    
<?php get_footer(); ?>