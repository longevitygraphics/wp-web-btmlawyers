<?php
   /*
   Plugin Name: Skunkworks Subpage Areas
   Description: A plugin for adding a Personal Injury custom post type to your WordPress theme.
   Version: 0.1
   Author: Skunkworks
   Author URI: http://skunkworks.ca
   License: GPL2
   */
// Subpage Areas

add_action( 'admin_menu', 'lg_subpages_menu' );

function lg_subpages_menu() {
  add_menu_page( 'Subpages', 'Subpages', 'manage_options', 'lg_subpages_menu', '', 'dashicons-portfolio', 6  );
}


    function subpages() {

    $labels = array(
        'name' => _x('Personal Injury Subpages', 'post type general name'),
        'singular_name' => _x('Personal Injury Subpage', 'post type singular name'),
        'add_new' => _x('Add New', 'Subpage'),
        'add_new_item' => __('Add New Subpage'),
        'edit_item' => __('Edit Subpage'),
        'new_item' => __('New Subpage'),
        'all_items' => __('Personal Injury Subpages'),
        'view_item' => __('View Subpage'),
        'search_items' => __('Search Subpages'),
        'not_found' =>  __('No Subpages found. Why not add some?'),
        'not_found_in_trash' => __('No Subpages found in trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Personal Injury Subpages'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => 'lg_subpages_menu',
        'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'page',
        'has_archive' => false,
        'menu_icon' => 'dashicons-portfolio',
        'rewrite' => array('slug' => 'services/personal-injury/-', 'with_front' => false),
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'revisions', 'author', 'thumbnail'),
        'can_export' => true,
        'menu_position' => 1
      );
    register_post_type('subpages',$args);


    register_post_type( 'employment-subpages',
      array(
        'labels' => array(
          'name' => ( 'Employment Subpages'),
          'singular_name' => ( 'Employment Subpage')
        ),
        'public' => true,
        'has_archive' => false,
        'rewrite' => array('slug' => 'services/employment/-', 'with_front' => false),
        'show_in_menu' => 'lg_subpages_menu',
        'supports' => array('title', 'editor', 'revisions', 'author', 'thumbnail'),
        'menu_position' =>2
      )
  );

  register_post_type( 'family-law-subpages',
      array(
        'labels' => array(
          'name' => ( 'Family Law Subpages'),
          'singular_name' => ( 'Family Law Subpage')
        ),
        'public' => true,
        'has_archive' => false,
        'rewrite' => array('slug' => 'services/family-law/-', 'with_front' => false),
        'show_in_menu' => 'lg_subpages_menu',
        'supports' => array('title', 'editor', 'revisions', 'author', 'thumbnail'),
        'menu_position' =>2
      )
  );



}


add_action('init', 'subpages');
?>
