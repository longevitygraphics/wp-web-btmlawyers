<?php
/**
 * Template : Personal Injury Subpages
 */

get_header(); ?>

    <?php if (get_field('contact_form') && !is_page('trademarks')): ?>
        <div class="internal-service-form subpage-top">
            <?php if (get_field('contact_form_title')): ?>
                <h2 class="interna-service-form-title"><?php echo get_field('contact_form_title'); ?></h2>
            <?php endif; ?>
            <div class="help-form-box">
                <?php echo do_shortcode(get_field('contact_form')); ?>
            </div>
        </div>
    <?php endif; ?>

<div class="content-wrap single-subpages">
    <div class="block bg-gray" style="padding-left:0;padding-right:0;">
        <div class="content">
            <!-- Intro Section -->
            <?php
                $introEnable = get_field('intro_enable');
                $introImage = get_field('intro_image');
                $introText = get_field('intro_text');
                $introCta = get_field('intro_cta_text');
            ?>

            <?php if($introEnable): ?>
                <div class="split-content block align-top">
                    <div class="half-image">
                        <?php if($introImage): ?>
                            <img src="<?php echo $introImage; ?>" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="half-copy">
                        <?php echo $introText; ?>
                       <div id="button" style="margin: 0;text-align: left;">
                            <a href="/contact/#contact-form"><?php echo $introCta; ?></a>
                       </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- end Intro Section -->

            <!-- FAQ: section1 -->
                <?php 
                    $faqTitle = get_field('faq_title');
                    $faqEnable = get_field('faq_enable');
                    $faqIndex = 0;
                ?>
                <div class="block" style="padding-left:0;padding-right:0;">
                    <h3>Here are some FAQs about <?php echo $title; ?> claims in BC we often get from our clients.</h3>

                    <?php if($faqEnable): ?>
                        <?php
                            if( have_rows('faqs') ):
                                while ( have_rows('faqs') && $faqIndex < 2 ) : the_row();
                                    $faqTitle = get_sub_field('title');
                                    $faqContent = get_sub_field('content');
                                    $faqIndex++;
                                    ?>
                                        <div class="faq <?php if($faqIndex == 1){echo 'active';} ?>">
                                            <h2><?php echo $faqTitle; ?></h2>
                                            <div class="faq-content" <?php if($faqIndex == 1){echo 'style="display:block;"';} ?>>
                                                <?php echo $faqContent; ?>
                                            </div>
                                        </div>
                                    <?php
                                endwhile;
                            else :
                                // no rows found
                            endif;
                        ?>
                    <?php endif; ?>
                </div>
            <!-- end FAQ: seciton1 -->
        </div>
    </div>
    

    <div class="block bg-white" style="padding-left:0;padding-right:0;">
        <div class="content">
            <!-- Feature FAQ Section -->
                <?php
                    $featureFaqEnable = get_field('feature_faq_enable');
                    $featureFaqTitle = get_field('feature_faq_title');
                    $featureFaqImage = get_field('feature_faq_image');
                    $featureFaqContent = get_field('feature_faq_content');
                ?>

                <?php if($featureFaqEnable): ?>
                    <div class="feature-faq-section">
                        <h3><?php echo $featureFaqTitle; ?></h3>
                        <div class="split-content vertical">
                            <div class="half-image">
                                <img src="<?php echo $featureFaqImage; ?>" alt="">
                            </div>
                            <div class="half-copy">
                                <?php echo $featureFaqContent; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <!-- end Feature FAQ Section -->

            <!-- FAQ: section2 -->
                <?php if($faqEnable): ?>
                    <?php
                        if( have_rows('faqs') ):
                            while ( have_rows('faqs') ) : the_row();
                                $faqTitle = get_sub_field('title');
                                $faqContent = get_sub_field('content');
                                ?>
                                    <div class="faq">
                                        <h2><?php echo $faqTitle; ?></h2>
                                        <div class="faq-content">
                                            <?php echo $faqContent; ?>
                                        </div>
                                    </div>
                                <?php
                            endwhile;
                        else :
                            // no rows found
                        endif;
                    ?>
                <?php endif; ?>
            <!-- end FAQ: seciton2 -->
        </div>
    </div>
</div>

<script>
(function($) {

    function scrollTo(offsetTop, time){
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, time);
    }

    function screenReposition(item){
        var windowHeight = $(window).height();
        var windowTop = $(window).scrollTop();
        var itemTop = item.offset().top;
        var itemHeight = item.outerHeight();
        var time = 500;
        var pos;

        if (item.is(':visible')){
            if(itemHeight > windowHeight){
                pos = itemTop;
            }else{
                pos = itemTop - (windowHeight - itemHeight) / 2;
            }
            scrollTo(pos, time);
        }
    }

    $(document).ready(function(){

        $('.faq').on('click',function(){
            var item = this;
            $('.faq').each(function(){
                if(item != this){
                    $(this)
                        .find('.faq-content')
                        .slideUp()
                        .closest('.faq')
                        .removeClass('active');
                }
            });

            $(this)
                .find('.faq-content')
                .slideToggle(function(){ screenReposition($(this).closest('.faq')); })
                .closest('.faq')
                .toggleClass('active');
        })
    });
}(jQuery));

</script>

<?php get_footer();
