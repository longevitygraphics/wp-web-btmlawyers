<?php get_header(); ?>



    <!-- START CONTENT -->
    <div class="content-wrap">
    <div class="content">

        
        
        
    <h1><?php the_title(); ?></h1>
    
        
        
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<div id="date-single"><?php the_time('F jS, Y') ?></div>
        
        <?php if (get_field('blog_post_alignment') == 'Left') { echo '<div class="left_aligned">'; } ?>
		<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
        
        <div id="category-single"><b>Posted in:</b><br><?php the_category(', '); ?></div>
        
        <?php if (get_field('blog_post_alignment') == 'Left') { echo '</div>'; } ?>
        
	<?php endwhile; endif; ?>
        
        
        
    <div id="button"><a href="<?php bloginfo('siteurl'); ?>/blog/">Back to blog</a></div>

        
        

    </div>
    </div>
    <!-- END CONTENT -->
    
    
    
<?php get_footer(); ?>