<?php get_header(); ?>

    <?php if (get_field('contact_form') && !is_page('trademarks')): ?>
        <div class="internal-service-form subpage-top">
            <?php if (get_field('contact_form_title')): ?>
                <h2 class="interna-service-form-title"><?php echo get_field('contact_form_title'); ?></h2>
            <?php endif; ?>
            <div class="help-form-box">
                <?php echo do_shortcode(get_field('contact_form')); ?>
            </div>
        </div>
    <?php endif; ?>


    <!-- START CONTENT -->
    <div class="content-wrap">
    <div class="content">


        
        
    <h1><?php the_title(); ?></h1>
    
        
        
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        
        <?php if (get_field('blog_post_alignment') == 'Left') { echo '<div class="left_aligned">'; } ?>
		<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
        
        <!-- <div id="category-single"><b>Posted in:</b><br><?php the_category(', '); ?></div> -->
        
        <?php if (get_field('blog_post_alignment') == 'Left') { echo '</div>'; } ?>
        
	<?php endwhile; endif; ?>
        
        
        
    <div id="button"><a href="/services/family-law/">Back to Family Law</a></div>

        
        

    </div>
    </div>
    <!-- END CONTENT -->
    
    
    
<?php get_footer(); ?>