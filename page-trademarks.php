<?php get_header(); ?>

    <style type="text/css">

        .trademark-banner {
            background-image: url(<?php bloginfo('template_directory'); ?>/images/trademark-banner.jpg);
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            height: 500px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .trademark-banner h1 {
            display: inline-block;
            margin: 0;
            padding: 0;
            font-size: 50px;
            color: white;
            font-weight: bold;
                text-shadow: 2px 2px 1px rgba(0, 0, 0, 0.3);
        }
        .trademark-banner svg {
            height: 50px;
            width: 50px;
            display: inline-block;
            margin-right: 16px;
        }
        .trademark-banner svg * {
            fill: white;
        }
        .trademark-page h2 {
            margin-bottom: 24px;
        }
        .trademark-page .white-bg {
            background-color: white;
            padding: 100px 0;
        }
        .trademark-page .white-bg.price-table {
            padding: 0;
        }
        .trademark-page .white-bg.price-table table {
            margin: auto;
            margin-top: 16px;
            margin-bottom: 16px;
        }
        .trademark-page .white-bg .content-wide {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
        }
        .trademark-page .white-bg .content-wide div {
            flex-basis: 45%;
        }
        @media screen and ( max-width: 800px ) {
            .trademark-page .white-bg .content-wide div {
                flex-basis: 100%;
            }
        }
        .trademark-page .white-bg .content-wide div * {
            text-align: left;
            margin: 0;
            padding: 0;
            max-width: 100%;
        }
        .trademark-page .white-bg .content-wide div h3 {
            margin-bottom: 16px;
            padding-top: 16px;
        }
        .trademark-page .white-bg .content-wide ol {
            padding-left: 16px!important;
        }
        .trademark-page .white-bg .content-wide ol li {
            list-style-position: outside;
            margin-bottom: 16px;
        }
        .trademark-page .white-bg .content-wide .trademark-full {
            flex-basis: 100%;
            margin: 0 auto;
            margin-bottom: 48px;
            max-width: 900px;
        }
        .trademark-page .white-bg .content-wide .trademark-full * {
            text-align: center;
        }
        .trademark-page .lawyer-teaser-trademark {
            max-width: 400px;
            margin: 48px auto;
        }
        .trademark-page .lawyer-teaser-trademark h2 {
            font-size: 24px;
            margin-bottom: 0;
        }
        .trademark-page .table-col {
            padding: 100px 16px;
        }
        .trademark-page table {
            max-width: 900px;
            margin: 48px auto;
            border-collapse: collapse;
            position: relative;
            font-family: 'Montserrat', sans-serif;
            color: rgb(87, 87, 87);
        }
        .trademark-page table:after {
            height: 2px;
            content: '';
            width: 100%;
            position: absolute;
            background-color: #474747;
            left: 0;
            bottom: 0;
        }
        .trademark-page table tr *:nth-child(1) {
            text-align: left;
        }
        .trademark-page table th {
            color: white;
            background-color: #474747;
        }
        .trademark-page table td, .trademark-page table th {
            padding: 8px;
            border: 2px solid white;
            height: 30px;
        }
        @media screen and ( max-width: 400px ) {
            .trademark-page table td, .trademark-page table th {
                font-size: 12px;
                padding: 4px;
            }
        }
    </style>

    <div class="trademark-banner">
    <span>
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33 34"><title>BTM-trademarks-icon-Trademarks icon</title><polygon points="15.91 3.79 19.64 15.29 31.72 15.29 21.95 22.39 25.68 33.88 15.91 26.78 6.13 33.88 9.87 22.39 0.09 15.29 12.17 15.29 15.91 3.79"/><path d="M30.2.86a5.33,5.33,0,0,1,2,2,5.37,5.37,0,0,1,.73,2.75,5.38,5.38,0,0,1-.73,2.74,5.58,5.58,0,0,1-2,2,5.47,5.47,0,0,1-5.53,0A5.48,5.48,0,0,1,22,5.63a5.38,5.38,0,0,1,.73-2.74,5.48,5.48,0,0,1,2-2,5.49,5.49,0,0,1,5.52,0Zm-.41,8.78a4.57,4.57,0,0,0,1.7-1.71,4.5,4.5,0,0,0,.63-2.33,4.56,4.56,0,0,0-.63-2.34,4.59,4.59,0,0,0-4-2.31,4.67,4.67,0,0,0-2.39.63A4.72,4.72,0,0,0,23.37,3.3a4.49,4.49,0,0,0-.62,2.33,4.63,4.63,0,0,0,4.67,4.65A4.53,4.53,0,0,0,29.79,9.64ZM29.47,5.7a1.75,1.75,0,0,1-.91.67l1.31,1.89H29l-1.1-1.74H26.16V8.26h-.8V2.67h2.18a2.46,2.46,0,0,1,1.66.5,1.74,1.74,0,0,1,.57,1.39A2,2,0,0,1,29.47,5.7Zm-1.93.16a1.64,1.64,0,0,0,1.1-.34,1.21,1.21,0,0,0,.39-1,1.11,1.11,0,0,0-.39-.91,1.7,1.7,0,0,0-1.1-.31H26.16V5.86Z"/></svg>
        <h1>Trademarks</h1>
        <?php if (have_rows("headings_cta")): ?>
        <div class="heading-ctas">
        <?php while(have_rows("headings_cta")): the_row(); ?>
            <?php $link = get_sub_field('link'); ?>
            <?php $link_icon = get_sub_field('link_icon'); ?>
            <?php if ($link): ?>
            <?php 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
             ?>
             <a class="heading-link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php if($link_icon){echo $link_icon; } ?><?php echo esc_html($link_title); ?></a>
            <?php endif ?>
        <?php endwhile; ?>
            </div>
        <?php endif ?>
    </span>
    </div>
    
    <?php if ( get_field('contact_form') ): ?>
        <div class="service-page-top">
            <div class="internal-service-form">
                <?php if (get_field('contact_form_title')): ?>
                    <h2 class="interna-service-form-title"><?php echo get_field('contact_form_title'); ?></h2>
                <?php endif ?>
                <div class="help-form-box">
                    <?php echo do_shortcode(get_field('contact_form')); ?>
                </div>
            </div>
        </div>
    <?php endif ?>
    

    <!-- START CONTENT -->
    <div class="content-wrap trademark-page">


        <div class="content-wide">
            <h2>Trademarks Team</h2>
            <?php
             $trademark_lawyer = get_post(45);
            // Get title and link
            $title = $trademark_lawyer->post_title;
            $link = get_permalink( $trademark_lawyer );
            // Get page ID from link for ACF
            $pageid = url_to_postid( $link );
            // Get advanced custom field data
            $indexcontent = get_field('index_excerpt', $pageid);
            $indeximage = get_field('index_image', $pageid);
            $lawyer_title = get_field( 'title', $pageid );
            /* ======= This was changed because WOW.js was not displaying lawyer bios in IE10 ======= */
            // echo '<div class="lawyer-teaser-large wow fadeInUp" data-wow-delay="400ms">';
            // echo '<div class="lawyer-teaser-large">'; /* This works in IE10 */
            echo '<div class="lawyer-teaser-trademark">'; /* Test for IE 10 */
            /* ============================= */
            echo '<div id="lawyer-teaser-photo"><a href="' . $link . '"><img src="' . $indeximage . '"></a></div>';
            echo '<div id="lawyer-teaser-content">';
            echo '<h2>' . $title;
            if ( $lawyer_title == 'Articled Student' ) { echo ' <span>' . $lawyer_title . '</span>' ; }
            echo '</h2>';
            echo '<p id="lawyer-teaser-paragraph">' . $indexcontent . '</p>';
            echo '<p><a href="' . $link . '">View bio</a></p>';
            echo '</div>';
            echo '</div>';
            ?>
        </div>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="white-bg">
            <div class="content-wide">
                <div class="trademark-full"><?php the_field('content_heading'); ?></div>
                <div><?php the_field('column_1'); ?></div>
                <div><?php the_field('column_2'); ?></div>
            </div>
        </div>
         <div class="white-bg price-table">
            <div class="content-wide">
                <?php $price_table = get_field("price_table"); ?>
                <?php echo $price_table; ?>
            </div>
        </div>

        <div class="content-wide table-col">
            <?php the_field('table'); ?>

<!-- The html table -->
<!--
<h2>Miscellaneous Trademark Tariff</h2>
<table width="100%">
    <tbody>
        <tr>
            <th width="54%">CANADA</th>
            <th width="15%">TMO FEE</th>
            <th width="14%">OUR FEE</th>
            <th width="14%">TOTAL<br />PLUS TAXES</th>
        </tr>
        <tr>
            <td width="54%">Transfer or Assignment</td>
            <td width="15%">$100</td>
            <td width="14%">$350</td>
            <td width="14%">$450</td>
        </tr>
        <tr>
            <td width="54%">Change of owner name (same owner)</td>
            <td width="15%">—</td>
            <td width="14%">$200</td>
            <td width="14%">$2000</td>
        </tr>
        <tr>
            <td width="54%">Form 7 Extension</td>
            <td width="15%">$450</td>
            <td width="14%">$750</td>
            <td width="14%">$1200</td>
        </tr>
        <tr>
            <td width="54%">Abandon TM after registration (immediate)</td>
            <td width="15%">—</td>
            <td width="14%">$150</td>
            <td width="14%">$150</td>
        </tr>
        <tr>
            <td width="54%">Abandon TM after registration (let die)</td>
            <td width="15%">—</td>
            <td width="14%">—</td>
            <td width="14%">—</td>
        </tr>
        <tr>
            <td width="54%"></td>
            <td width="15%"></td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%">Certified Copy of Registration</td>
            <td width="15%">$50</td>
            <td width="14%">$150</td>
            <td width="14%">$200</td>
        </tr>
        <tr>
            <td width="54%"></td>
            <td width="15%"></td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%">Renewal (15 years)</td>
            <td width="15%">$350</td>
            <td width="14%">$500</td>
            <td width="14%">$850</td>
        </tr>
        <tr>
            <td width="54%">Extension for Declaration of Use</td>
            <td width="15%">$125</td>
            <td width="14%">$200</td>
            <td width="14%">$325</td>
        </tr>
        <tr>
            <td width="54%">Section 45 Notice</td>
            <td width="15%">$400</td>
            <td width="14%">Hourly</td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%">Statement of Objection</td>
            <td width="15%">$1000</td>
            <td width="14%">Hourly</td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%">License Agreement</td>
            <td width="15%">—</td>
            <td width="14%">$750 min</td>
            <td width="14%">$750</td>
        </tr>
        <tr>
            <td width="54%"></td>
            <td width="15%"></td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <th width="54%">USA<br />(most USPTO fees are PER CLASS)</th>
            <th width="15%">USPTO FEE</th>
            <th width="14%">OUR FEE</th>
            <th width="14%">TOTAL<br />PLUS TAXES</th>
        </tr>
        <tr>
            <td width="54%">Transfer or Assignment</td>
            <td width="15%">$40 + per</td>
            <td width="14%">$350</td>
            <td width="14%">$290</td>
        </tr>
        <tr>
            <td width="54%">Change of owner name (same owner)</td>
            <td width="15%"></td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%"></td>
            <td width="15%"></td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%">Section 8 Maintenance of US Trademark (5-6 years)</td>
            <td width="15%">$125</td>
            <td width="14%">$500</td>
            <td width="14%">$600</td>
        </tr>
        <tr>
            <td width="54%">Sections 8 and 9 Combined (Every 10 years)</td>
            <td width="15%">$425</td>
            <td width="14%">$675</td>
            <td width="14%">$1100</td>
        </tr>
        <tr>
            <td width="54%">Section 15 Incontestibility</td>
            <td width="15%">$200</td>
            <td width="14%">$300</td>
            <td width="14%">$500</td>
        </tr>
        <tr>
            <td width="54%">Sections 8 and 15 Combined (5-6 years)</td>
            <td width="15%">$300</td>
            <td width="14%">$675</td>
            <td width="14%">$975</td>
        </tr>
        <tr>
            <td width="54%">Any above within grace period</td>
            <td width="15%">+$100-200</td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%"></td>
            <td width="15%"></td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%">Extension for Statement of Use</td>
            <td width="15%">$150</td>
            <td width="14%">$200</td>
            <td width="14%">$350</td>
        </tr>
        <tr>
            <td width="54%">Response to Suspension Notice/Inquiry</td>
            <td width="15%">—</td>
            <td width="14%">$150</td>
            <td width="14%">$150</td>
        </tr>
        <tr>
            <td width="54%"></td>
            <td width="15%"></td>
            <td width="14%"></td>
            <td width="14%"></td>
        </tr>
        <tr>
            <td width="54%">Abandon TM after registration (immediate)</td>
            <td width="15%">—</td>
            <td width="14%">$150</td>
            <td width="14%">$150</td>
        </tr>
        <tr>
            <td width="54%">Abandon TM after registration (let die)</td>
            <td width="15%">—</td>
            <td width="14%">—</td>
            <td width="14%">—</td>
        </tr>
        <tr>
            <td width="54%">License Agreement (Canada/US)</td>
            <td width="15%">—</td>
            <td width="14%">$750 min</td>
            <td width="14%">$750</td>
        </tr>
    </tbody>
</table>

<p>Fees are subject to change without notice.  US fees above are subject to exchange rates.
Fees in the US generally increase by $200-300 per class.</p>
-->

        </div>

    <?php endwhile; endif; ?>

    </div>
    <!-- END CONTENT -->



<?php get_footer(); ?>
