<?php
/*
Template Name: Bio
*/
?>


<?php get_header(); ?>



    <!-- START CONTENT -->
    <div class="content-wrap">
    <div class="content">


    <!-- START BIO SOCIAL -->
    <div class="bio-social-wrap">

    <?php if( get_field( 'facebook') != '' ) : ?><a href="<?php echo get_field( 'facebook' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-facebook.png"></a><?php endif; ?>
    <?php if( get_field( 'twitter') != '' ) : ?><a href="<?php echo get_field( 'twitter' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-twitter.png"></a><?php endif; ?>
    <?php if( get_field( 'linkedin') != '' ) : ?><a href="<?php echo get_field( 'linkedin' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-linkedin.png"></a><?php endif; ?>
    <?php if( get_field( 'google') != '' ) : ?><a href="<?php echo get_field( 'google' ); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/social-icon-google.png"></a><?php endif; ?>

    </div>
    <!-- END BIO SOCIAL -->



    <div class="bio-content">

    <div class="bio-intro"><?php echo get_field( 'intro' ); ?></div>

    <div id="divide"></div>




        <?php
            // Find connected pages
            $connected = new WP_Query( array(
              'connected_type' => 'services_to_lawyers',
              'connected_items' => get_queried_object(),
              'nopaging' => true,
              'orderby' => 'title',
              'order' => 'asc'
            ) );

            // Display connected pages
            if ( $connected->have_posts() ) :  ?>
            <h3>Practice Areas</h3>
            <ul>
            <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>

            <li><a href="<?php the_permalink()?>"><?php the_title()?></a></li>



            <?php endwhile; ?>
            </ul>
            <?php wp_reset_postdata();
            endif; ?>




    <?php echo get_field( 'content' ); ?>

    </div>


    </div>
    </div>
    <!-- END CONTENT -->




    <!-- START BIO CONTACT -->
    <div class="bio-contact-wrap">
    <div class="bio-contact">

    <div class="bio-contact-item">Direct: <?php echo get_field( 'direct' ); ?></div>
    <?php if( get_field( 'cell') != '' ) : ?><div class="bio-contact-item">Cell: <?php echo get_field( 'cell' ); ?></div><?php endif; ?>
    <?php if( get_field( 'fax') != '' ) : ?><div class="bio-contact-item">Fax: <?php echo get_field( 'fax' ); ?></div><?php endif; ?>
    <div class="bio-contact-item"><a href="mailto:<?php echo get_field( 'email' ); ?>"><?php echo get_field( 'email' ); ?></div></a>

    </div>
    </div>
    <!-- END BIO CONTACT -->



    <?php if( get_field( 'assistant_name') != '' ) : ?>
    <!-- START BIO ASSISTANT -->
    <div class="bio-assistant-wrap">
    <div class="bio-assistant">

    <h3>Assistant<?php if( get_field( 'second_assistant_name') != '' ) : ?>s<?php endif; ?></h3>
    <p>
    <b><?php echo get_field( 'assistant_name' ); ?></b>
    <?php if( get_field( 'assistant_phone') != '' ) : ?><br><?php echo get_field( 'assistant_phone' ); ?><?php endif; ?>
    <?php if( get_field( 'assistant_email') != '' ) : ?><br><a href="mailto:<?php echo get_field( 'assistant_email' ); ?>"><?php echo get_field( 'assistant_email' ); ?></a><?php endif; ?>
    <?php if( get_field( 'assistant_practice_areas') != '' ) : ?><br><i><?php echo get_field( 'assistant_practice_areas' ); ?></i><?php endif; ?>
    </p>

    <?php if( get_field( 'second_assistant_name') != '' ) : ?><p>
    <b><?php echo get_field( 'second_assistant_name' ); ?></b>
    <?php if( get_field( 'second_assistant_phone') != '' ) : ?><br><?php echo get_field( 'second_assistant_phone' ); ?><?php endif; ?>
    <?php if( get_field( 'second_assistant_email') != '' ) : ?><br><a href="mailto:<?php echo get_field( 'second_assistant_email' ); ?>"><?php echo get_field( 'second_assistant_email' ); ?></a><?php endif; ?>
    <?php if( get_field( 'second_assistant_practice_areas') != '' ) : ?><br><i><?php echo get_field( 'second_assistant_practice_areas' ); ?></i><?php endif; ?>
    </p>
    <?php endif; ?>

    <?php if( get_field( 'third_assistant_name') != '' ) : ?><p>
    <b><?php echo get_field( 'third_assistant_name' ); ?></b>
    <?php if( get_field( 'third_assistant_phone') != '' ) : ?><br><?php echo get_field( 'third_assistant_phone' ); ?><?php endif; ?>
    <?php if( get_field( 'third_assistant_email') != '' ) : ?><br><a href="mailto:<?php echo get_field( 'third_assistant_email' ); ?>"><?php echo get_field( 'third_assistant_email' ); ?></a><?php endif; ?>
    <?php if( get_field( 'third_assistant_practice_areas') != '' ) : ?><br><i><?php echo get_field( 'third_assistant_practice_areas' ); ?></i><?php endif; ?>
    </p>
    <?php endif; ?>

    <?php if( get_field( 'fourth_assistant_name') != '' ) : ?><p>
    <b><?php echo get_field( 'fourth_assistant_name' ); ?></b>
    <?php if( get_field( 'fourth_assistant_phone') != '' ) : ?><br><?php echo get_field( 'fourth_assistant_phone' ); ?><?php endif; ?>
    <?php if( get_field( 'fourth_assistant_email') != '' ) : ?><br><a href="mailto:<?php echo get_field( 'fourth_assistant_email' ); ?>"><?php echo get_field( 'fourth_assistant_email' ); ?></a><?php endif; ?>
    <?php if( get_field( 'fourth_assistant_practice_areas') != '' ) : ?><br><i><?php echo get_field( 'fourth_assistant_practice_areas' ); ?></i><?php endif; ?>
    </p>
    <?php endif; ?>

    <?php if( get_field( 'fifth_assistant_name') != '' ) : ?><p>
    <b><?php echo get_field( 'fifth_assistant_name' ); ?></b>
    <?php if( get_field( 'fifth_assistant_phone') != '' ) : ?><br><?php echo get_field( 'fifth_assistant_phone' ); ?><?php endif; ?>
    <?php if( get_field( 'fifth_assistant_email') != '' ) : ?><br><a href="mailto:<?php echo get_field( 'fifth_assistant_email' ); ?>"><?php echo get_field( 'fifth_assistant_email' ); ?></a><?php endif; ?>
    <?php if( get_field( 'fifth_assistant_practice_areas') != '' ) : ?><br><i><?php echo get_field( 'fifth_assistant_practice_areas' ); ?></i><?php endif; ?>
    </p>
    <?php endif; ?>

    </div>
    </div>
    <!-- END BIO ASSISTANT -->
    <?php endif; ?>



<?php get_footer(); ?>
