<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?>>

<head>

<meta http-equiv=Content-Type content="text/html;  charset=ISO-8859-1">

<title><?php wp_title(); ?></title>

<meta charset="<?php bloginfo( 'charset' ); ?>" />

<!-- UPGRADE INSECURE REQUESTS - https://developers.google.com/web/fundamentals/security/prevent-mixed-content/fixing-mixed-content -->
<?php
    if ( home_url() == "https://www.btmlawyers.com" || home_url() == "https://btmlawyers.com") {
        echo '<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">';
    }
?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<meta name="viewport" content="width=460">-->

<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php bloginfo('template_url'); ?>/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="<?php bloginfo('template_url'); ?>/favicon/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">

<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
<link href='<?php bloginfo('template_url'); ?>/font-ionicons/css/ionicons.min.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>



<!-- ELEMENT ANIMATIONS - http://daneden.github.io/animate.css/ -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/animate/animate.css">

<script src="<?php bloginfo('template_url'); ?>/animate/wow.min.js"></script>
<script>
 new WOW().init();
</script>



<?php if(is_front_page()) /* If Home Page */ :?>

<!-- SMOOTH SCROLLING SCRIPT FOR HOME PAGE -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 800);
        return false;
      }
    }
  });
});
</script>

<?php endif;?>




<?php wp_head(); ?>




<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style.css">




</head>

<body>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5X7N67"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5X7N67');</script>
<!-- End Google Tag Manager -->

<?php if(is_front_page()) /* If Home Page */ :?>


<style type="text/css">
#fillScreen {
    background-image: url('<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); echo $src[0]; ?>');
    background-repeat: no-repeat;
    background-position: top center;
    background-size: cover;
}
</style>

<!-- START HEADER IMAGE SIZING -->
<div id="fillScreen">



    <!-- START TOP HEADER -->
    <div class="header-home-wrap">
    <div class="header-home">

    <div id="logo-home"><a href="<?php bloginfo('siteurl'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/BTM-lawyers-logo.png" alt="Personal Injury Lawyer & Business Law Firm| BTM Lawyers" ></a></div>

    <div class="primary-nav-home">
    <?php wp_nav_menu( array('menu' => 'Primary Menu', 'container_class' => 'primary-menu' )); ?>
    </div>

    </div>
    </div>
    <!-- END TOP HEADER -->



    <!-- START TOP HEADING -->
    <div class="home-intro-wrap">

    <div id="home-heading-top" class="wow fadeIn" data-wow-delay="100ms"><?php the_field('main_heading'); ?></div>
    <div id="home-heading-bottom" class="wow fadeIn" data-wow-delay="200ms"><?php the_field('sub_heading'); ?></div>
    <div id="home-button" class="wow fadeIn" data-wow-delay="300ms" style="margin: auto 10px;"><a href="<?php the_field('button_link'); ?>"><span class="fa fa-mobile fa-2x valign-button-icon"></span> <?php the_field('button_text'); ?></a></div>
    <div id="home-button" class="wow fadeIn" data-wow-delay="400ms"><a href="mailto:<?php echo get_field( 'email_link' ); ?>"><span class="fa fa-envelope fa-2x valign-button-icon"></span> Email Us</a></div>
    <?php if ( get_field('button2_text') ) { ?>
    <div id="home-button" class="wow fadeIn" data-wow-delay="350ms" style="margin: auto 10px;"><a href="<?php the_field('button2_link'); ?>"><span class="fa fa-info-circle fa-2x valign-button-icon"></span> <?php the_field('button2_text'); ?></a></div>
    <?php } ?>

    <div class="mobile-buttons-only">

    <div id="home-button" class="wow fadeIn" data-wow-delay="500ms"><a href="<?php echo get_field( 'map_link' ); ?>" target="_blank"><span class="fa fa-map-marker fa-2x valign-button-icon"></span> Find Us</a></div>
    </div>

    </div>
    <!-- END TOP HEADING -->



    <a class="btn-dwn wow slideInDown" href="#makeItScroll" data-wow-delay="800ms"><span class="fa fa-chevron-down"></span></a>



</div>
<!-- END HEADER IMAGE SIZING -->


<!-- START CONTENT BELOW HEADER IMAGE -->
<div id="makeItScroll">



<?php else /* If NOT Home Page */ :?>



    <!-- START TOP HEADER -->
    <div class="header-internal-wrap">
    <div class="header-internal">

    <div id="logo-internal"><a href="<?php bloginfo('siteurl'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/BTM-lawyers-logo-internal.png"></a></div>

    <div class="primary-nav-internal">
    <?php wp_nav_menu( array('menu' => 'Primary Menu', 'container_class' => 'primary-menu' )); ?>
    </div>

    </div>
    </div>
    <!-- END TOP HEADER -->



<?php endif;?>


<?php if(is_page(17)) /* If Personal Injury Page */ :?>

    <!-- START HEADER IMAGE -->
    <div class="header-image-large-wrap">

    <div class="internal-heading-wrap">
        <div id="internal-heading-top" class="wow fadeIn" data-wow-delay="200ms"><?php the_field('heading'); ?></div>
        <div id="internal-heading-bottom" class="wow fadeIn" data-wow-delay="400ms"><?php the_field('sub_heading'); ?></div>

        <div id="pi-subpages">
            <?php
            $args = array(
                'orderby'          => 'title',
                'order'            => 'ASC',
                'author'           => 1,
                'posts_per_page'   => -1,
                'post_type'        => 'subpages',
                'post_status'      => 'publish',
                'suppress_filters' => true
            );
            $subPagesArray = get_posts( $args );
            foreach ($subPagesArray as $spage) {
                $sTitle = get_field('link_title', $spage->ID);
                echo '<a class="pi-subpage-link wow fadeIn" href="'.get_permalink($spage->ID).'">'.$sTitle.'</a>';
            }
            ?>
        </div>
    </div>


    <style type="text/css">

    #pi-button-wrap {
        background-color: #f0f0f0;
        margin-bottom: -16px;
        padding-top: 16px;
    }
    .pi-button {
        display: inline-block;
    }
    .pi-button a {
        color: #104b7d!important;
        border-color: #104b7d!important;
    }
    #pi-subpages {
    }
    .pi-subpage-link {
        font-family: 'Montserrat',sans-serif;
        font-weight: bold;
        font-size: 18px;
        display: inline-block;
        margin: 8px;
        padding: 20px;
        border: 3px solid #fff;
        border-radius: 5px;
        /*border-top-left-radius: 0;*/
        background-color: transparent /*#104b7d*/;
        color: white;
        text-decoration: none;
    }
    .pi-subpage-link:hover {
        color: #104b7d /*#c5c5c5*/;
        background-color: #fff;
    }
    @media screen and (max-width: 600px){
    .header-image-large-wrap {
    background-image: url('<?php bloginfo('template_url'); ?>/videos/pi.jpg');
    background-repeat: no-repeat;
    background-position: bottom left;
    background-size: cover;
    }
        .pi-subpage-link {
            padding: 12px;
        }
    }
    </style>


    <!-- Start EasyHtml5Video.com BODY section -->
    <style type="text/css">
    video {
    position: absolute;
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: 1;
    }
    </style>


    <video autoplay="autoplay" muted poster="<?php bloginfo('template_url'); ?>/videos/transparent.png" title="BTM Lawyers" loop="loop" onended="var v=this;setTimeout(function(){v.play()},300)">
    <source src="<?php bloginfo('template_url'); ?>/videos/pi.m4v" type="video/mp4" />
    <source src="<?php bloginfo('template_url'); ?>/videos/pi.webm" type="video/webm" />
    </video>
    <script src="<?php bloginfo('template_url'); ?>/videos/html5ext.js" type="text/javascript"></script>
    <!-- End EasyHtml5Video.com BODY section -->



    </div>
    <!-- END HEADER IMAGE -->
    <div id="pi-button-wrap">
        <div id="internal-button" class="pi-button wow fadeIn" data-wow-delay="300ms"><a href="<?php the_field('button_link', 2); ?>"><span class="fa fa-mobile fa-2x valign-button-icon"></span> <?php the_field('button_text', 2); ?></a></div>
        <div id="internal-button" class="pi-button wow fadeIn" data-wow-delay="600ms"><a href="<?php the_field('button_link'); ?>"><?php the_field('button'); ?></a></div>
    </div>


<?php endif;?>









<?php if(is_page(11)) /* If About Page */ :?>

    <!-- START HEADER IMAGE -->
    <div class="header-image-large-wrap">

    <div class="internal-heading-wrap">
    <div id="internal-heading-top" class="wow fadeIn" data-wow-delay="200ms"><?php the_field('heading'); ?></div>
    <div id="internal-heading-bottom" class="wow fadeIn" data-wow-delay="400ms"><?php the_field('sub_heading'); ?></div>
    </div>


    <style type="text/css">

    @media screen and (max-width: 600px){
    .header-image-large-wrap {
    background-image: url('<?php bloginfo('template_url'); ?>/videos/about.jpg');
    background-repeat: no-repeat;
    background-position: bottom left;
    background-size: cover;
    }

    }
    </style>



    <!-- Start EasyHtml5Video.com BODY section -->
    <style type="text/css">
    video {
    position: absolute;
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: 1;
    }
    </style>


    <video autoplay="autoplay" muted poster="<?php bloginfo('template_url'); ?>/videos/transparent.png" title="BTM Lawyers" loop="loop" onended="var v=this;setTimeout(function(){v.play()},300)">
    <source src="<?php bloginfo('template_url'); ?>/videos/about.m4v" type="video/mp4" />
    <source src="<?php bloginfo('template_url'); ?>/videos/about.webm" type="video/webm" />
    </video>
    <script src="<?php bloginfo('template_url'); ?>/videos/html5ext.js" type="text/javascript"></script>
    <!-- End EasyHtml5Video.com BODY section -->



    </div>
    <!-- END HEADER IMAGE -->

<?php endif;?>





<?php if(is_page(15)) /* If Contact Page */ :?>

    <!-- START HEADER IMAGE -->
    <div class="header-image-large-wrap">

    <div class="internal-heading-wrap">
    <div id="internal-heading-top" class="wow fadeIn" data-wow-delay="200ms"><?php the_field('heading'); ?></div>
    <div id="internal-heading-bottom" class="wow fadeIn" data-wow-delay="400ms"><?php the_field('sub_heading'); ?></div>
    <div id="home-button" class="wow fadeIn" data-wow-delay="600ms"><a href="<?php the_field('button_link'); ?>"><span class="fa fa-mobile fa-2x valign-button-icon"></span> <?php the_field('button_text'); ?></a></div>
    <div id="home-button" class="wow fadeIn" data-wow-delay="400ms"><a href="mailto:<?php echo get_field( 'email_link',2 ); ?>"><span class="fa fa-envelope fa-2x valign-button-icon"></span> Email Us</a></div>
    </div>


    <!-- Start Google Street View BODY section -->
    <style>
	    html, body {
	        height: 100%;
	        margin: 0px;
	        padding: 0px
	    }
	    #map-canvas {
	        min-height:560px;
	        height:760px;
	    }

	    .gm-iv-address {
	       display:none;
	    }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script>
			function initialize() {
			  var btmlawyers = new google.maps.LatLng(49.280523,-122.828346);
			  var panoramaOptions = {
			    position: btmlawyers,
			    streetViewControl: false,
			    linksControl: false,
			    panControl: false,
			    zoomControl:false,
			    scrollwheel: false,

			    pov: {
			      heading: 152,
			      pitch: 10
			    },
			    zoom: 1
			  };
			  var myPano = new google.maps.StreetViewPanorama(
			      document.getElementById('map-canvas'),
			      panoramaOptions);
			  myPano.setVisible(true);
			}

			google.maps.event.addDomListener(window, 'load', initialize);

    </script>



        <div id="map-canvas"></div>


    <!-- End Google Street View BODY section -->



    </div>
    <!-- END HEADER IMAGE -->

<?php endif;?>





<?php if(is_tree(9) && !is_page(9)) /* If Team is Parent and NOT Team Page  */ :?>

    <style type="text/css">
    .bio-image-header-wrap {
        background-image: url('<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); echo $src[0]; ?>');
        background-repeat: no-repeat;
        background-position: top center;
        background-size: cover;
    }
    </style>

    <!-- START HEADER IMAGE -->
    <div class="bio-image-header-wrap">

    <div class="bio-image-header-bottom-overlay">

    <h1 class="wow fadeIn" data-wow-delay="200ms"><?php the_title(); ?></h1>
    <h3 class="wow fadeIn" data-wow-delay="400ms"><?php echo get_field( 'education' ); ?> - <span id="tan"><?php echo get_field( 'title' ); ?></span></h3>

    <div id="bio-button-wrap" class="wow fadeIn" data-wow-delay="600ms">
    <div id="button"><a href="tel:+1<?php echo get_field( 'direct' ); ?>"><span class="fa fa-mobile fa-lg valign-button-icon"></span> <?php echo get_field( 'direct' ); ?></a></div>
    <div id="button"><a href="mailto:<?php echo get_field( 'email' ); ?>"><span class="fa fa-envelope fa-fw valign-button-icon-email"></span> Email</a></div>
    </div>

    </div>

    </div>
    <!-- END HEADER IMAGE -->

<?php endif;?>







<?php if(is_tree(7) && !is_page(7) || get_post_type() == 'subpages') /* If Services is Parent and NOT Service Page */ :?>

    <?php if(!is_page(17)) /* If NOT Personal Injury */ :?>


    <?php if(has_post_thumbnail()) :?>

    <style type="text/css">
    .header-image-service-wrap {
        background-image: url('<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); echo $src[0]; ?>');
        background-repeat: no-repeat;
        background-position: center center;
        background-size: cover;
        padding: 220px 0px 220px 0px;
    }

    .header-image-service-wrap h1 {
        padding: 0;
        text-shadow: 2px 2px 1px rgba(0, 0, 0, 0.3);
    }
    </style>

    <?php else :?>

    <style type="text/css">
    .header-image-service-wrap {
        background-image: url('<?php bloginfo('template_url'); ?>/images/service-header-bg.jpg');
        background-repeat: no-repeat;
        background-position: top center;
        background-size: cover;
    }
    </style>

    <?php endif;?>


    <!-- START HEADER IMAGE -->
    <div class="header-image-service-wrap">

    <?php if(get_post_type() == 'subpages'): ?>
        <h1 class="wow fadeIn" data-wow-delay="200ms"><?php the_title(); ?></h1>
        <style>
            .header-image-service-wrap {
                padding: 120px 0px 120px 0px;
            }
        </style>
        <div id="home-button">
            <a href="/contact/#contact-form">Book free consultation</a>
        </div>
    <?php else: ?>
        <h1 class="wow fadeIn" data-wow-delay="200ms"><span class="<?php echo get_field( 'icon' ); ?> fa-lg service-button-icon"></span><?php the_title(); ?></h1>
    <?php endif; ?>

    </div>
    <!-- END HEADER IMAGE -->

    <?php endif;?>

<?php endif;?>
