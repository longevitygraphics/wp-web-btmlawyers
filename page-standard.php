<?php
/*
Template Name: Standard
*/
?>


<?php get_header(); ?>



    <!-- START HEADER IMAGE -->
    <div class="header-image-wrap">
        
    <h1><?php the_title(); ?></h1>
    
    <?php if( get_field('sub-heading') ): ?><h2><?php the_field('sub-heading'); ?></h2><?php endif; ?>
        
    <div id="spacer"></div>
        
    <?php if( get_field('button_text') && get_field('button_link') ): ?><div id="internal-button" class="wow fadeIn" data-wow-delay="300ms"><a href="<?php the_field('button_link'); ?>"><?php the_field('button_text'); ?></a></div><?php endif; ?>
    
        
    <?php if(has_post_thumbnail()) :?>
            
    <style type="text/css">
    .header-image-wrap {
        background-image: url('<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' ); echo $src[0]; ?>');
        background-repeat: no-repeat;
        background-position: center center;
        background-size: cover;
    }
    </style>
        
    <?php endif;?>
    
    </div>
    <!-- END HEADER IMAGE -->



    <!-- START CONTENT -->
    <div class="content-wrap">
    
    <div class="content-wide">
            
            


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
    
    
        <?php if (is_page(913))  /* If Dont be a Dummy Page */  { ?>
            
        <?php
            // Find connected pages
            $connected = new WP_Query( array(
              'connected_type' => 'services_to_lawyers',
              'connected_items' => get_queried_object(),
              'nopaging' => true,
            ) );

            // Display connected pages
            if ( $connected->have_posts() ) :  ?>
            <h2 style="margin-top: 50px;">Personal Injury Team</h2>
            <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
            
            <?php
               // Get title and link
                $title = $post->post_title;
                $link = get_permalink( $child );

                // Get page ID from link for ACF
                $pageid = url_to_postid( $link );

                // Get advanced custom field data
                $indexcontent = get_field('index_excerpt', $pageid); 
                $indeximage = get_field('index_image', $pageid);
                $lawyer_title = get_field( 'title', $pageid );
                                                                      
                /* ======= This was changed because WOW.js was not displaying lawyer bios in IE10 ======= */
                // echo '<div class="lawyer-teaser-large wow fadeInUp" data-wow-delay="400ms">';
                // echo '<div class="lawyer-teaser-large">'; /* This works in IE10 */
                echo '<div class="lawyer-teaser-large animated fadeInUp">'; /* Test for IE 10 */
                /* ============================= */


                echo '<div id="lawyer-teaser-photo"><a href="' . $link . '"><img src="' . $indeximage . '"></a></div>';
                echo '<div id="lawyer-teaser-content">';
                echo '<h3 style="text-align:left;margin:0;">' . $title;
                if ( $lawyer_title == 'Articled Student' ) { echo ' <span>' . $lawyer_title . '</span>' ; }
                echo '</h3>';
                echo '<p id="lawyer-teaser-paragraph">' . $indexcontent . '</p>';
                echo '<p><a href="' . $link . '">View bio</a></p>';
                echo '</div>';
                echo '</div>';
            ?>
            
            <?php endwhile; ?>
            

            <?php wp_reset_postdata();
            endif; ?> 
            
            <?php if( get_field('button_text') && get_field('button_link') ): ?><div id="internal-button" class="wow fadeIn blue" data-wow-delay="300ms"><a href="<?php the_field('button_link'); ?>"><?php the_field('button_text'); ?></a></div><?php endif; ?>
        
        <?php } ?>
        
	<?php endwhile; endif; ?>
        

        

    </div>
    </div>
    <!-- END CONTENT -->
    
    
    
<?php get_footer(); ?>