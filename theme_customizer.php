<?php

new theme_customizer();

class theme_customizer
{
    
    public function __construct()
    {
        add_action( 'customize_register', array(&$this, 'sw_customize_manager' ));
        
        add_action('customize_register', 'themename_customize_register');
        function themename_customize_register($wp_customize) {
            $wp_customize->remove_section( 'title_tagline' );
            $wp_customize->remove_section( 'nav' );
            $wp_customize->remove_section( 'static_front_page' );
        }
    }

    public function sw_customize_manager( $wp_manager )
    {
        $this->demo_section( $wp_manager );
    }

    public function demo_section( $wp_manager )
    {

        
        
        
        
        // About Teaser
        $wp_manager->add_section( 'about-teaser', array(
            'title'          => 'About Teaser',
            'priority'       => 29,
        ) );

            // Heading
            $wp_manager->add_setting( 'about-heading', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'about-heading', array(
                'label'   => 'Heading',
                'section' => 'about-teaser',
                'type'    => 'text',
                'priority' => 1
            ) );
        
            // Heading
            $wp_manager->add_setting( 'about-content', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'about-content', array(
                'label'   => 'Content',
                'section' => 'about-teaser',
                'type'    => 'textarea',
                'priority' => 2
            ) );
        
            // Button
            $wp_manager->add_setting( 'about-button', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'about-button', array(
                'label'   => 'Button',
                'section' => 'about-teaser',
                'type'    => 'text',
                'priority' => 3
            ) );
        
            // Button Link
            $wp_manager->add_setting( 'about-button-link', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'about-button-link', array(
                'label'   => 'Button Link',
                'section' => 'about-teaser',
                'type'    => 'text',
                'priority' => 4
            ) );
        
        
        
        
        
        
        // Personal Injury Teaser
        $wp_manager->add_section( 'pi-teaser', array(
            'title'          => 'Personal Injury Teaser',
            'priority'       => 30,
        ) );

            // Heading
            $wp_manager->add_setting( 'heading', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'heading', array(
                'label'   => 'Heading',
                'section' => 'pi-teaser',
                'type'    => 'text',
                'priority' => 1
            ) );
        
            // Heading
            $wp_manager->add_setting( 'content', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'content', array(
                'label'   => 'Content',
                'section' => 'pi-teaser',
                'type'    => 'textarea',
                'priority' => 2
            ) );
        
            // Button
            $wp_manager->add_setting( 'button', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'button', array(
                'label'   => 'Button',
                'section' => 'pi-teaser',
                'type'    => 'text',
                'priority' => 3
            ) );
        
            // Button Link
            $wp_manager->add_setting( 'button-link', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'button-link', array(
                'label'   => 'Button Link',
                'section' => 'pi-teaser',
                'type'    => 'text',
                'priority' => 4
            ) );
 
        
        
        
        
        
        
        // Footer Contact
        $wp_manager->add_section( 'footer-contact', array(
            'title'          => 'Footer Contact',
            'priority'       => 31,
        ) );

            // Address
            $wp_manager->add_setting( 'address', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'address', array(
                'label'   => 'Address',
                'section' => 'footer-contact',
                'type'    => 'textarea',
                'priority' => 1
            ) );
        
            // Telephone Number
            $wp_manager->add_setting( 'telephone', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'telephone', array(
                'label'   => 'Telephone',
                'section' => 'footer-contact',
                'type'    => 'text',
                'priority' => 2
            ) );
        
            // Fax Number
            $wp_manager->add_setting( 'fax', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'fax', array(
                'label'   => 'Fax',
                'section' => 'footer-contact',
                'type'    => 'text',
                'priority' => 3
            ) );
        
            // Email
            $wp_manager->add_setting( 'email', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'email', array(
                'label'   => 'Email',
                'section' => 'footer-contact',
                'type'    => 'text',
                'priority' => 4
            ) );
        
            // Button 1
            $wp_manager->add_setting( 'button-1', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'button-1', array(
                'label'   => 'Button 1',
                'section' => 'footer-contact',
                'type'    => 'text',
                'priority' => 5
            ) );
        
            // Button 1 Link
            $wp_manager->add_setting( 'button-1-link', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'button-1-link', array(
                'label'   => 'Button 1 Link',
                'section' => 'footer-contact',
                'type'    => 'text',
                'priority' => 6
            ) );
        
            // Button 2
            $wp_manager->add_setting( 'button-2', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'button-2', array(
                'label'   => 'Button 2',
                'section' => 'footer-contact',
                'type'    => 'text',
                'priority' => 7
            ) );
        
            // Button 2 Link
            $wp_manager->add_setting( 'button-2-link', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'button-2-link', array(
                'label'   => 'Button 2 Link',
                'section' => 'footer-contact',
                'type'    => 'text',
                'priority' => 8
            ) );
        
        
        
        
        
        

        // Footer
        $wp_manager->add_section( 'footer', array(
            'title'          => 'Footer',
            'priority'       => 32,
        ) );

            // Slogan
            $wp_manager->add_setting( 'slogan', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'slogan', array(
                'label'   => 'Slogan',
                'section' => 'footer',
                'type'    => 'textarea',
                'priority' => 1
            ) );
        
            // Facebook Link
            $wp_manager->add_setting( 'social_facebook', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'social_facebook', array(
                'label'   => 'Facebook Link',
                'section' => 'footer',
                'type'    => 'text',
                'priority' => 2
            ) );
        
            // Twitter Link
            $wp_manager->add_setting( 'social_twitter', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'social_twitter', array(
                'label'   => 'Twitter Link',
                'section' => 'footer',
                'type'    => 'text',
                'priority' => 3
            ) );
        
            // LinkedIn Link
            $wp_manager->add_setting( 'social_linkedin', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'social_linkedin', array(
                'label'   => 'LinkedIn Link',
                'section' => 'footer',
                'type'    => 'text',
                'priority' => 4
            ) );
        
            // Google Link
            $wp_manager->add_setting( 'social_google', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'social_google', array(
                'label'   => 'Google Link',
                'section' => 'footer',
                'type'    => 'text',
                'priority' => 5
            ) );
        
            // Copyright
            $wp_manager->add_setting( 'copyright', array(
                'default'        => '',
            ) );
            $wp_manager->add_control( 'copyright', array(
                'label'   => 'Copyright',
                'section' => 'footer',
                'type'    => 'text',
                'priority' => 6
            ) );
        
            





    }

}

?>